﻿using UnityEngine;

public class CardFactory : MonoBehaviour
{
    public Card Card;
    public GameObject CardContainer;

    public Material Circle, Cross, Diamond, Heart, Lightning, Shield, Star, Triangle;

    public Material RedSuit, GreenSuit, BlueSuit, GraySuit;

    public Card CreateCard(Symbol symbol, Suit suit)
    {
        Card card = Instantiate(Card,CardContainer.transform,false);

        switch (symbol)
        {
            case Symbol.Circle:
                card.Symbol.material = Circle;
                break;
            case Symbol.Cross:
                card.Symbol.material = Cross;
                break;
            case Symbol.Diamond:
                card.Symbol.material = Diamond;
                break;
            case Symbol.Heart:
                card.Symbol.material = Heart;
                break;
            case Symbol.Lightning:
                card.Symbol.material = Lightning;
                break;
            case Symbol.Shield:
                card.Symbol.material = Shield;
                break;
            case Symbol.Star:
                card.Symbol.material = Star;
                break;
            case Symbol.Triangle:
                card.Symbol.material = Triangle;
                break;
        }

        switch (suit)
        {
            case Suit.Red:
                card.Suit.material = RedSuit;
                break;
            case Suit.Green:
                card.Suit.material = GreenSuit;
                break;
            case Suit.Blue:
                card.Suit.material = BlueSuit;
                break;
            case Suit.Gray:
                card.Suit.material = GraySuit;
                break;
        }

        card.CurrentSuit = suit;
        card.CurrentSymbol = symbol;

        card.transform.name = "Card: " + symbol;
        return card;
    }
}
