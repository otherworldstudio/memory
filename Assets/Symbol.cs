﻿public enum Symbol
{
    None,
    Circle,
    Cross,
    Diamond,
    Heart,
    Lightning,
    Shield,
    Star,
    Triangle
}
