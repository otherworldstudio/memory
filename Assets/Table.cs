﻿using Assets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.EventSystems;

public class Table : MonoBehaviour
{
    private CardFactory cardFactory;
    private TextController textController;
    public Camera MainCamera;

    public List<Card> Cards;
    public Suit SelectedSuit;

    public Vector2 GameGridSize = new Vector2(4, 4);
    private Vector2 PairsStack = new Vector2(-3.26f, 3.12f);

    public Card[] revealedCards = new Card[2];

    private Vector3 solvedStackPosition = new Vector3(-3.27f, 3.125f, -2);
    private bool isPair = false;

    public int guessesDone = 0;
    public int goodGuessesDone = 0;
    public int wrongGuessesDone = 0;
    public int wrongGuessesAllowed = 0;

    private void Start()
    {
        cardFactory = GetComponent<CardFactory>();
        textController = GetComponent<TextController>();

        textController.Restart.onClick.AddListener(RestartButton);

        Cards = new List<Card>();
        
        SetupGame();
    }

    private void Update()
    {
        DetectMouseClicks();

        CardPairing();

        WinCondition();
    }


    private void WinCondition()
    {
        if (Cards.Any(c => c.Solved == false)) return;
        textController.SetGameOverPanel(true);
            }

    private void DetectMouseClicks()
    {
        var mouseWorldPosition = MainCamera.ScreenToWorldPoint(Input.mousePosition);
        var hitInfo = Physics2D.Raycast(mouseWorldPosition, Vector2.zero);

        if (hitInfo.transform == null)
        {
            return;
        }

        var card = hitInfo.transform.GetComponent<Card>();

        if (card != null)
        {
            if (Input.GetMouseButtonDown(0) && (revealedCards[0] == null || revealedCards[1] == null) && !EventSystem.current.IsPointerOverGameObject())
            {
                card.SetState(true);

                if (revealedCards[0] != null && revealedCards[1] == null && revealedCards[0].transform.position != card.transform.position)
                {
                    revealedCards[1] = card;
                    guessesDone++;

                    textController.UpdateGuessesDone(guessesDone);
                }
                if (revealedCards[0] == null)
                {
                    revealedCards[0] = card;
                }
            }
        }

    }

    private void CardPairing()
    {
        if (revealedCards[0] == null || revealedCards[1] == null) return;
        if (revealedCards[0].Revealed == true && revealedCards[1].Revealed == true)
        {
            isPair = CheckIfPair(revealedCards[0], revealedCards[1]);
        }

        if (isPair)
        {
            revealedCards[0].SetState(false);
            revealedCards[1].SetState(false);

            if (revealedCards[0].Revealed == false && revealedCards[1].Revealed == false)
            {
                revealedCards[0].transform.position = solvedStackPosition;
                revealedCards[1].transform.position = solvedStackPosition;

                revealedCards[0].Solved = true;
                revealedCards[1].Solved = true;

                revealedCards[0] = null;
                revealedCards[1] = null;

                isPair = false;

                goodGuessesDone++;
            }
        }
        else if (!isPair && revealedCards[0].Revealed == true && revealedCards[1].Revealed == true)
        {
            wrongGuessesDone++;
            textController.UpdateWrongGuessesAllowed(wrongGuessesAllowed - wrongGuessesDone > 0 ? wrongGuessesAllowed - wrongGuessesDone : 0);
            revealedCards[0].SetState(false);
            revealedCards[1].SetState(false);

            revealedCards[0] = null;
            revealedCards[1] = null;
        }
    }

    private bool CheckIfPair(Card first, Card second)
    {
        if (first != null && second != null)
        {
            if (first.CurrentSymbol == second.CurrentSymbol)
            {
                return true;
            }
        }
        return false;
    }

    public void RestartButton()
    {
        SetupGame();
    }

    public void SetupGame()
    {
        if (Cards.Count > 0)
        {
            for (int i = 0; i < Cards.Count; i++)
            {
                Cards[i].DestroyYourself();
            }
        }

        Cards = new List<Card>();
        
        guessesDone = 0;
        goodGuessesDone = 0;
        wrongGuessesDone = 0;


        textController.SetGameOverPanel(false);
        textController.UpdateGuessesDone(guessesDone);
        textController.UpdateWrongGuessesAllowed(wrongGuessesAllowed);

        var possibleCards = new List<Symbol>
        {
            Symbol.Circle,
            Symbol.Cross,
            Symbol.Diamond,
            Symbol.Heart,
            Symbol.Lightning,
            Symbol.Shield,
            Symbol.Star,
            Symbol.Triangle,
            Symbol.Circle,
            Symbol.Cross,
            Symbol.Diamond,
            Symbol.Heart,
            Symbol.Lightning,
            Symbol.Shield,
            Symbol.Star,
            Symbol.Triangle
        };

        possibleCards.Shuffle();

        var cardQueue = new Queue<Symbol>(possibleCards);

        for (var x = 0; x < GameGridSize.x; x++)
        {
            for (var y = 0; y < GameGridSize.y; y++)
            {
                var symbolForNewCard = cardQueue.Dequeue();
                var newCard = cardFactory.CreateCard(symbolForNewCard, SelectedSuit);

                newCard.transform.position = new Vector3((x * 0.8f + 0.4f) - (GameGridSize.x / 2 * 0.8f), (y * 1.05f + 0.55f) - (GameGridSize.y / 2 * 1.05f), -1);

                Cards.Add(newCard);
            }
        }
    }
}
