﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour {

    public Text guessesDone;
    public Text wrongGuessesAllowed;

    public Text gameOver;
    public Button Restart;

    public GameObject gameOverPanel;
    
    public void SetGameOverPanel(bool state)
    {
        gameOverPanel.SetActive(state);
    }

    public void UpdateGuessesDone(int guessesDoneAmount)
    {
        guessesDone.text = guessesDoneAmount.ToString();
    }

    public void UpdateWrongGuessesAllowed(int wrongGuessesAllowedAmount)
    {
        wrongGuessesAllowed.text = wrongGuessesAllowedAmount.ToString();
    }
}
