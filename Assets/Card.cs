﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour {
    public Animator animator;

    public MeshRenderer Suit;
    public MeshRenderer Symbol;

    public Suit CurrentSuit;
    public Symbol CurrentSymbol;

    public bool Revealed;
    public bool Solved;

    public void SetState(bool state)
    {
        if(!Solved)
        animator.SetBool("Revealed",state);
    }

    public bool GetState()
    {
        return animator.GetBool("Revealed");
    }

    public bool RevealCard()
    {
        return Revealed = true;
    }

    public bool HideCard()
    {
        return Revealed = false;
    }

    public void DestroyYourself()
    {
        Destroy(gameObject);
    }
}
